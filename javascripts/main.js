$(document).ready(function()
{
	//
  // Plugin input text select
  //
  //
  $.fn.inputTextSelect = function( options )
  {
    var wrapper = $(this);
    var input   = $( '.text-select-input', wrapper );
    var select  = $( '.text-select-options', wrapper );

    //Set options
    var defaults =
    {
      inputTextNumber: true,
      onInputFocus: function( input ){},
      onInputKeyup: function( input ){},
      onInputChange: function(){},
      onOptionSelect: function(){},
      beforeLoad: function(){},
      afterLoad: function(){}
    };
    var vars = $.extend({}, defaults, options);

    //if the letter is not digit then display error and don't type anything
    if ( vars.inputTextNumber ) {
      input.keypress(function (e)
      {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
          return false;
        }
      });
    }

    var mouseInOptions = false;
    var mouseInInput   = false;

    //
    //Input focusin
    input.focusin(function (e)
    {
      var value = $(this).val();

      mouseInInput = true;
      //Hide all options
      $( '.text-select-select' ).removeClass( 'open' );
      //Run vars function
      vars.onInputFocus( input, value );
      //Open current options
      select.addClass( 'open' );
    });

    //
    //Input focusout
    input.focusout(function (e)
    {
      mouseInInput = false;
    });

    //
    //Input keyup
    input.keyup(function (e)
    {
      var value = $(this).val();
      //Run vars function
      vars.onInputKeyup( input, value );
    });

    //
    //Fade menu options
    //Jika mouse di atas options
    select.hover( function(){
      mouseInOptions = true;
    }, function(){
      mouseInOptions = false;
    });
    $( "body" ).click(function(){
      if ( ! mouseInOptions && ! mouseInInput ) {
        select.removeClass( 'open' );
      }
    });

    //
    //Options click
    $( 'li', select ).click( function(e)
    {
      var label = $(this).text();
      var value = $(this).data( 'value' );

      //set input
      input.val( label );
      input.attr( 'data-value-real', value );

      //Run vars function
      vars.onOptionSelect( $(this), value );

      //Close current options
      select.removeClass( 'open' );

      return false;
    });
  };
  // End inputTextSelect

  //
  //Input lama menginap
  $( '.night-spend' ).inputTextSelect(
  {
    inputTextNumber: false,
    selectLabel: true,
    onOptionSelect: function( input, value )
    {
      $('.date-hotel-to').datepicker('destroy');

      var longint  = new Number( value );
      var fromdate = new Date(hotelFromSelect.getFullYear(), hotelFromSelect.getMonth(), hotelFromSelect.getDate(), 0, 0, 0, 0);
      var todate   = new Date(fromdate.getFullYear(), fromdate.getMonth(), fromdate.getDate(), 0, 0, 0, 0);
      todate.addDays( longint );
      var maxdate  = new Date(fromdate.getFullYear(), fromdate.getMonth(), fromdate.getDate(), 0, 0, 0, 0);
      maxdate.addDays( 15 );

      //Change date to
      var toDateText = todate.getDate() + '-' + (todate.getMonth()+1) + '-' + todate.getFullYear();
      $( '.date-hotel-to' ).val( toDateText );
      dateHotelTo = $( '.date-hotel-to' ).datepicker(
      {
        format: 'dd-mm-yyyy',
        beforeShowDay: function(date)
        {
          var dateval = date.valueOf();
          if ( dateval < fromdate.valueOf() || dateval > maxdate.valueOf() ) {
            return 'disabled';
          } else {
            return '';
          }
        }
      })
      .on('changeDate', function(ev) {
        dateHotelTo.hide();
      })
      .data('datepicker');
    }
  });

  //
  //Input kamar
  $( '.text-select-room' ).inputTextSelect();

  //
  //Input passeger
  var maxPassenger = 10;
  if ( $( '#ticket .wraper-passenger' ).length > 0 ) {
    $( '#ticket .wraper-passenger' ).each(function()
    {
      $( this ).inputTextSelect(
      {
        onInputFocus: function( input, value )
        {
          var wrapper = input.parents( '.wraper-passenger' );
          var select  = $( '.text-select-options', wrapper );
          var total   = _getPassegerTotal();
          var valInt  = new Number( value );

          if ( (total - valInt) < maxPassenger )
          {
            var count = maxPassenger - total + valInt;
            var opt   = $( 'ul', select ).html('');

            for ( var i = 0; i <= count; i++ )
            {
              var val = i;
              var li  = $('<li data-value="' + val + '">' + val + '</li>');
              li.click( function(e)
              {
                var wrp = $(this).parents( '.wraper-passenger' );
                var val = $(this).data( 'value' );
                //set input
                $( '.text-select-input', wrapper ).val( val );
                //Hide opt
                select.removeClass( 'open' );

                if ( wrp.hasClass( 'passenger-adult' ) ) {
                  if ( val > 0 ) {
                    $('.passenger-baby .text-select-input').removeAttr( 'disabled' );
                  } else {
                    $('.passenger-baby .text-select-input').attr( 'disabled', 'disabled' );
                    $('.passenger-baby .text-select-input').val( 0 );
                  }
                }
                return false;
              });
              opt.append( li );
            }
          }
        },
        //On key up
        onInputKeyup: function( input, value )
        {
          _checkPassenger( input, value );
        }
      });
    });
  }
  //
  //Check passenger select
  function _checkPassenger( input, value )
  {
    var wrapper = input.parents( '.wraper-passenger' );
    var select  = $( '.text-select-options', wrapper );
    //Clear message
    $( '.passenger-message' ).removeClass( 'right' );
    $( '.passenger-message' ).removeClass( 'center' );
    $( '.passenger-message' ).hide();

    if ( value != '' ) {
      select.removeClass( 'open' );
    } else {
      select.addClass( 'open' );
    }

    //Check adult
    var valAdult = $('.passenger-adult .text-select-input').val();
    var valAdInt = new Number( valAdult );

    if ( wrapper.hasClass( 'passenger-adult' ) ) {
      if ( valAdInt > 0 ) {
        $('.passenger-baby .text-select-input').removeAttr( 'disabled' );
      } else {
        $('.passenger-baby .text-select-input').attr( 'disabled', 'disabled' );
        $('.passenger-baby .text-select-input').val( 0 );
      }
    }
    else {
      if ( valAdInt < 0 ) {
        return false;
      }
    }

    var total = _getPassegerTotal();
    if ( total > maxPassenger )
    {
      input.val('');
      $( '.passenger-message' ).removeClass( 'right' );
      $( '.passenger-message' ).removeClass( 'center' );
      $( '.passenger-message' ).show();

      if ( wrapper.hasClass( 'passenger-child' )) {
        $( '.passenger-message' ).addClass( 'center' );
      }
      else if ( wrapper.hasClass( 'passenger-baby' )) {
        $( '.passenger-message' ).addClass( 'right' );
      }
    }
  }
  //
  //Get total passeger
  function _getPassegerTotal()
  {
    var adult = $('.passenger-adult .passenger-form').val();
    var baby  = $('.passenger-baby .passenger-form').val();
    var child = $('.passenger-child .passenger-form').val();

    var adultInt = babyInt = childInt = 0;
    if ( adult != '0' ) {
      adultInt = new Number( adult );
    }
    if ( baby != '0' ) {
      babyInt = new Number( baby );
    }
    if ( child != '0' ) {
      childInt = new Number( child );
    }
    return adultInt + babyInt + childInt;
  }

  //
  //start animation velocity
  $('.small-text-slider').velocity({translateY: -100,opacity: 1, duration: 500});
  $('.big-text-slider').delay(500).velocity({translateY: -100,opacity: 1, duration: 500, sequenceQueue: false});
  $('.button-order-now').delay(800).velocity({translateY: -100,opacity: 1,duration: 500, sequenceQueue: false});
  // main slider plugin
  $('.flexslider').flexslider({
      animation: "fade",
      slideshow: false,
      after: function(){
        $('.small-text-slider').delay(50).velocity({translateY: -100,opacity: 1,  duration: 500, sequenceQueue: false});
        $('.big-text-slider').delay(200).velocity({translateY: -100,opacity: 1, duration: 500, sequenceQueue: false});
        $('.button-order-now').delay(300).velocity({translateY: -100,opacity: 1, duration: 500, sequenceQueue: false});
      },
      before: function(){
        $('.small-text-slider').delay(80).velocity({translateY: 0,opacity: 0,  duration: 500, sequenceQueue: false});
        $('.big-text-slider').delay(100).velocity({translateY: 0,opacity: 0, delay: 250, duration: 500, sequenceQueue: false});
        $('.button-order-now').velocity({translateY: 0,duration: 500, opacity: 0, stagger: 250, sequenceQueue: false});
      },
  });

  //centering modal bootstrap
  var modalVerticalCenterClass = ".modal";
  function centerModals($element) {
    var $modals;
    if ($element.length) {
        $modals = $element;
    } else {
        $modals = $(modalVerticalCenterClass + ':visible');
    }
    $modals.each( function(i) {
        var $clone = $(this).clone().css('display', 'block').appendTo('body');
        var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
        top = top > 0 ? top : 0;
        $clone.remove();
        $(this).find('.modal-content').css("margin-top", top);
    });
  }
  $(modalVerticalCenterClass).on('show.bs.modal', function(e) {
      centerModals($(this));
  });
  $(window).on('resize', centerModals);

  //
  //Date picker
  var nowTemp  = Date.today().addDays(1);
  var now      = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
  var nextTemp = Date.today().next().year().addDays(1);
  var nextYear = new Date(nextTemp.getFullYear(), nextTemp.getMonth(), nextTemp.getDate(), 0, 0, 0, 0);
  var nextDay  = Date.today().addDays(1);
  var nowDay   = now.getDate();
  var nowMon   = now.getMonth()+1;
  var nowYear  = now.getFullYear();
  var ndayDay  = nextDay.getDate();
  var ndayMon  = nextDay.getMonth()+1;
  var ndayYear = nextDay.getFullYear();

  //
  //Date ticket to
  $( '.date-picker-to' ).val( ndayDay + '-' + ndayMon + '-' + ndayYear );
  var dateTo = $( '.date-picker-to' ).datepicker(
  {
    format: 'dd-mm-yyyy',
    beforeShowDay: function(date)
    {
      var dateval = date.valueOf();
      if ( dateval < now.valueOf() || dateval > nextYear.valueOf() ) {
        return 'disabled';
      } else {
        return '';
      }
    }
  })
  .on('changeDate', function(ev)
  {
    dateTo.hide();
  })
  .data('datepicker');

  //
  //Date ticket from
  $( '.date-picker-from' ).val( ndayDay + '-' + ndayMon + '-' + ndayYear );
  var dateFrom = $( '.date-picker-from' ).datepicker(
  {
    format: 'dd-mm-yyyy',
    beforeShowDay: function(date)
    {
      var dateval = date.valueOf();
      if ( dateval < now.valueOf() || dateval > nextYear.valueOf() ) {
        return 'disabled';
      } else {
        return '';
      }
    }
  })
  .on('changeDate', function(ev)
  {
    $('.date-picker-to').datepicker('destroy');
    var stemp = ev.date;
    var sdate = new Date(stemp.getFullYear(), stemp.getMonth(), stemp.getDate(), 0, 0, 0, 0);

    //Change date to
    var toDate = stemp.getDate() + '-' + (stemp.getMonth()+1) + '-' + stemp.getFullYear();
    $( '.date-picker-to' ).val( toDate );
    dateTo = $( '.date-picker-to' ).datepicker(
    {
      format: 'dd-mm-yyyy',
      beforeShowDay: function(date)
      {
        var dateval = date.valueOf();
        if ( dateval < stemp.valueOf() || dateval > nextYear.valueOf() ) {
          return 'disabled';
        } else {
          return '';
        }
      }
    })
    .on('changeDate', function(ev) {
      dateTo.hide();
    })
    .data('datepicker');

    //Hide datepicker
    dateFrom.hide();
  })
  .data('datepicker');

  //
  //
  //Date hotel to
  var hotelFromSelect = now;
  var day01 = new Date(nextDay.getFullYear(), nextDay.getMonth(), nextDay.getDate(), 0, 0, 0, 0);
  day01.addDays(1);
  var day15 = new Date(nextDay.getFullYear(), nextDay.getMonth(), nextDay.getDate(), 0, 0, 0, 0);
  day15.addDays(15);
  $( '.date-hotel-to' ).val( day01.getDate() + '-' + (day01.getMonth()+1) + '-' + day01.getFullYear() );
  var dateHotelTo = $( '.date-hotel-to' ).datepicker(
  {
    format: 'dd-mm-yyyy',
    beforeShowDay: function(date)
    {
      var dateval = date.valueOf();
      if ( dateval < day01.valueOf() || dateval > day15.valueOf() ) {
        return 'disabled';
      } else {
        return '';
      }
    }
  })
  .on('changeDate', function(ev)
  {
    var fromDay = hotelFromSelect.valueOf();
    var toDay   = ev.date.valueOf();
    var long    = (toDay - fromDay)/(1000*60*60*24);

    $( '.long-night' ).attr( 'data-value-real', long );
    $( '.long-night' ).val( long + ' Malam' );

    //Hide datepicker
    dateHotelTo.hide();
  })
  .data('datepicker');

  //
  //
  //Date hotel form
  $( '.date-hotel-from' ).val( nowDay + '-' + nowMon + '-' + nowYear );
  var dateHotelFrom = $( '.date-hotel-from' ).datepicker(
  {
    format: 'dd-mm-yyyy',
    beforeShowDay: function(date)
    {
      var dateval = date.valueOf();
      if ( dateval < now.valueOf() || dateval > nextYear.valueOf() ) {
        return 'disabled';
      } else {
        return '';
      }
    }
  })
  .on('changeDate', function(ev)
  {
    $('.date-hotel-to').datepicker('destroy');

    var long    = $( '.long-night' ).attr( 'data-value-real' );
    var longint = new Number( long );
    var stemp   = ev.date;
    var sdate   = new Date(stemp.getFullYear(), stemp.getMonth(), stemp.getDate(), 0, 0, 0, 0);
    hotelFromSelect = sdate;

    var seldate = new Date(stemp.getFullYear(), stemp.getMonth(), stemp.getDate(), 0, 0, 0, 0);
    seldate.addDays(longint);
    var date15  = new Date(seldate.getFullYear(), seldate.getMonth(), seldate.getDate(), 0, 0, 0, 0);
    date15.addDays(15);

    //Change date to
    var toDate = seldate.getDate() + '-' + (seldate.getMonth()+1) + '-' + seldate.getFullYear();
    $( '.date-hotel-to' ).val( toDate );
    dateHotelTo = $( '.date-hotel-to' ).datepicker(
    {
      format: 'dd-mm-yyyy',
      beforeShowDay: function(date)
      {
        var dateval = date.valueOf();
        if ( dateval < seldate.valueOf() || dateval > date15.valueOf() ) {
          return 'disabled';
        } else {
          return '';
        }
      }
    })
    .on('changeDate', function(ev) {
      dateHotelTo.hide();
    })
    .data('datepicker');

    //Hide datepicker
    dateHotelFrom.hide();
  })
  .data('datepicker');

  //
  //Sekali jalan
  $( '.radio-way-once' ).change( function()
  {
    $( '.wraper-each-input.bottom.date' ).hide();
  });
  $( '.radio-way-twice' ).change( function()
  {
    $( '.wraper-each-input.bottom.date' ).show();
  });

  //
  //Input city ticket
  //
  $( '.wraper-input-city' ).each( function()
  {
    var wraper = $(this);

    $( '.input-city-main', wraper ).autocomplete({
      lookup: function (query, done)
      {
        wraper.addClass( 'onload' );

        $.ajax({
          url: 'data/get_cities.php',
          type: "POST",
          dataType: "json",
          data: 'q=' + query,
          success: function (results)
          {
            var data = {
              'suggestions': []
            };
            if ( results )
            {
              for( var i = 0; i < results.length; i++ )
              {
                data.suggestions[i] = {
                  'value': results[i].name,
                  'data': results[i].id
                }
              }
              done( data );
            }
            wraper.removeClass( 'onload' );
          }
        });
      },
      onSelect: function (suggestion) {
        $( '.input-city-value', wraper ).val( suggestion.data );
      }
    });
  });

  //
  //Input hotel destination
  //
  $( '.wraper-input-hotel-destination' ).each( function()
  {
    var wraper = $(this);

    $( '.input-city-main', wraper ).autocomplete(
    {
      containerClass: 'autocomplete-suggestions-hotel',
      lookup: function (query, done)
      {
        wraper.addClass( 'onload' );

        $.ajax({
          url: 'data/get_destination.php',
          type: "POST",
          dataType: "json",
          data: 'q=' + query,
          success: function (results)
          {
            var data = {
              'suggestions': []
            };
            if ( results )
            {
              for( var i = 0; i < results.length; i++ )
              {
                data.suggestions[i] = {
                  'value': results[i].name,
                  'data': results[i].id,
                  'type': results[i].type
                }
              }
              done( data );
            }
            wraper.removeClass( 'onload' );
          }
        });
      },
      onSelect: function (suggestion) {
        $( '.input-city-value', wraper ).val( suggestion.data );
      },
      formatResult: function( suggestion, currentValue )
      {
        var pattern = '(' + autocompleteUtils.escapeRegExChars(currentValue) + ')';

        var label = suggestion.value
            .replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>')
            .replace(/&/g, '&amp;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/"/g, '&quot;')
            .replace(/&lt;(\/?strong)&gt;/g, '<$1>');

        var option = label + '<div class="suggestion-type">' + suggestion.type + '</div>';
        return option;
      }
    });
  });

  var autocompleteUtils = (function()
  {
    return {
        escapeRegExChars: function (value) {
          return value.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
        },
        createNode: function (containerClass) {
          var div = document.createElement('div');
          div.className = containerClass;
          div.style.position = 'absolute';
          div.style.display = 'none';
          return div;
        }
    };
  }());

  //
  //Form subscribe
  //

  //name rule
  jQuery.validator.addMethod("validname", function(value, element) {
    return this.optional(element) || /^[a-zA-Z\s]*$/.test(value);
  }, "Nama tidak valid, nama hanya dapat mengandung huruf dan spasi.");

  jQuery.validator.addMethod("validemail", function(value, element) {
    return this.optional(element) || /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(value);
  }, "Please enter a valid email address.");
  //login validate

  //Main validate
  $("#form-email-subscribe").validate(
  {
    errorClass: "form-field-error",
    errorElement: "div",
    errorPlacement: function( error, element )
    {
        var field = element.parents( '.form-field' );
        field.addClass( 'form-field-invalid' );
        if ( field.hasClass( 'form-field-inline' )) {
            field.before( error );
        } else {
            element.before( error );
        }
    },
    highlight: function( element, errorClass, validClass )
    {
        $(element)
            .parents( '.form-field' )
            .addClass( 'form-field-invalid' );
    },
    unhighlight: function(element, errorClass, validClass)
    {
      $(element)
          .parents( '.form-field' )
          .removeClass( 'form-field-invalid' );

    },
		focusCleanup: true,
    onkeyup: false,
    rules: {
      username: {
        required: true,
        validname: true
      },
      email: {
        required: true,
        validemail: true
      },
    },
    messages: {},
    submitHandler: function(form) {
      form.submit();
    }
 });

  //
  //Form login
  //
  $("#login-form").validate(
  {
    errorClass: "form-field-error",
    errorElement: "div",
    errorPlacement: function( error, element )
    {
        var field = element.parents( '.form-field' );
        field.addClass( 'form-field-invalid' );
        if ( field.hasClass( 'form-field-inline' )) {
            field.before( error );
        } else {
            element.before( error );
        }
    },
    highlight: function( element, errorClass, validClass )
    {
        $(element)
            .parents( '.form-field' )
            .addClass( 'form-field-invalid' );
    },
    unhighlight: function(element, errorClass, validClass)
    {
      $(element)
          .parents( '.form-field' )
          .removeClass( 'form-field-invalid' );

    },
		focusCleanup: true,
    onkeyup: false,
    rules: {
      email: {
        required: true,
        validemail: true
      },
      pass: {
        required: true
      },
    },
    messages: {},
    submitHandler: function(form) {
      form.submit();
    }
 });

  //
  //Register login
  //
  $("#register-form").validate(
  {
    errorClass: "form-field-error",
    errorElement: "div",
    errorPlacement: function( error, element )
    {
        var field = element.parents( '.form-field' );
        field.addClass( 'form-field-invalid' );
        if ( field.hasClass( 'form-field-inline' )) {
            field.before( error );
        } else {
            element.before( error );
        }
    },
    highlight: function( element, errorClass, validClass )
    {
        $(element)
            .parents( '.form-field' )
            .addClass( 'form-field-invalid' );
    },
    unhighlight: function(element, errorClass, validClass)
    {
      $(element)
          .parents( '.form-field' )
          .removeClass( 'form-field-invalid' );

    },
		focusCleanup: true,
    onkeyup: false,
    rules: {
      email: {
        required: true,
        validemail: true
      }
    },
    messages: {},
    submitHandler: function(form)
		{
      //form.submit();

			$('#modal-login').modal('hide');
			$('#modal-thanks').modal('show');
    }
 });

  //
  //Personal data form
  //
  $("#personal-data-form").validate(
  {
    errorClass: "form-field-error",
    errorElement: "div",
    errorPlacement: function( error, element )
    {
        var field = element.parents( '.personal-field' );
        field.addClass( 'form-field-invalid' );
        if ( field.hasClass( 'form-field-inline' )) {
            field.after( error );
        } else {
            element.after( error );
        }
    },
    highlight: function( element, errorClass, validClass )
    {
        $(element)
            .parents( '.personal-field' )
            .addClass( 'form-field-invalid' );
    },
    unhighlight: function(element, errorClass, validClass)
    {
      $(element)
          .parents( '.personal-field' )
          .removeClass( 'form-field-invalid' );

    },
		focusCleanup: true,
    onkeyup: false,
    rules: {
      password: {
        required: true
      },
      password_confirm: {
        required: true
      },
      title: {
        required: true
      },
      firstname: {
        required: true
      },
      lastname: {
        required: true
      },
      phone: {
        required: true
      }
    },
    messages: {},
    submitHandler: function(form)
		{
      form.submit();
    }
 });


//menu mobile navigations
  //function toogle menu when open
    $('#nav-button').click(function() {
    if ($(this).hasClass('open')){
        $(this).removeClass('open');
        $('.main-body').removeClass('fixed');
    } else {
        $(this).addClass('open');
         $('.main-body').addClass('fixed');
      }
});
  // function on and off sequence when menu clicked
   var buttonOn=false;
   var $push_body = $(".navicon-button"),
       $add_height = $("#mobile-navigation"),
       $overlay_mobile = $(".mobile-overlay"),
       $push_menu = $(".mobile-list-menu");

    var onSequence = [
        { e: $push_body,  p: {translateX: [145,0]},  o: { duration: 200,  sequenceQueue: false }},
        { e: $add_height,  p: {height: "100%"},  o: { duration: 100,  sequenceQueue: false }},
        { e: $overlay_mobile,  p: {opacity: 1},  o: { duration: 300,  sequenceQueue: false, visibility: "visible" }},
        { e: $push_menu, p: {translateX: [250,0]}, o: { duration: 350,  delay: 70, sequenceQueue: false }}

    ];

    var offSequence = [

        { e: $push_body, p: {translateX: [0,145]}, o: { duration: 350, sequenceQueue: false }},
        { e: $add_height,  p: {height: "0%"},  o: { duration: 500,  sequenceQueue: false }},
        { e: $overlay_mobile,  p: {opacity: 0},  o: { duration: 300,  sequenceQueue: false, visibility: "hidden" }},
        { e: $push_menu, p: {translateX: [0,250]}, o: { duration: 350, sequenceQueue: false }},


    ];
    // on off button
    $(".navicon-button").click(
      function() {
        buttonOn = !buttonOn;
        if (buttonOn) {
         $.Velocity.RunSequence(onSequence);
        } else {$.Velocity.RunSequence(offSequence);
        }
      });

    $(".main-modal-login-click").click(function() {
        $.Velocity.RunSequence(offSequence);
      });
    $(".mobile-overlay").click(function() {
        $.Velocity.RunSequence(offSequence);
        $('.main-body').removeClass('fixed');
        $("#nav-button").removeClass('open');
        buttonOn = false;
      });

  //popup partner
  var $modal_overlay = $(".overlay-big-modal");
  var showPopup = [
    { e: $modal_overlay, p: {opacity: 1, scale:[1]}, o: { duration: 200,  sequenceQueue: false }}
  ];

  var hidePopup = [
    { e: $modal_overlay, p: {opacity: 0, scale:[0]}, o: { duration: 200,  sequenceQueue: false }},
  ];

  //click overlay function
  $("#show-partner").click(
  function() {
		var inscrollWidth = $(window).width();
		$('body').css('overflow', 'hidden');
		var nonScrollWidth = $('body').width();
		$('body').css('overflow', 'auto');
		var scrollWidth = nonScrollWidth - inscrollWidth;

     $.Velocity.RunSequence(showPopup);
     $('body').addClass("hide-over");
	 	 $('body').css('overflow', 'hidden');
		 $('body').css( 'padding-right', scrollWidth );
  });
  $("#close-partner").click(
    function(){
    $.Velocity.RunSequence(hidePopup);
    $('body').removeClass("hide-over");
	 $('body').css('overflow', 'auto');
		$('body').css( 'padding-right', 0 );
  });

   //init wow js
   new WOW().init();

   //
   //Faq collapse
   $('#faq #accordion').on('shown.bs.collapse', function (el)
   {
     var item  = $(el.target).parents( '.panel' );
     var totop = item.offset().top;
     $("html, body").animate({ scrollTop: totop }, 400);
   });

   //smooth scroll
   $("#down-scroll").click(function() {
    $('html, body').animate({
      scrollTop: $("#polygon-section").offset().top
    }, 500);
  });

});
