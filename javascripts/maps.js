function initialize() {
        var latlng = new google.maps.LatLng(-6.1947943,106.8211289);
        var myOptions = {
            zoom: 17,
            draggable: false,
            scrollwheel: false,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map_div"),
                myOptions);

        var image = 'images/map-locator.png';
        var image = {
            url: 'images/map-locator.png',
            size: new google.maps.Size(200, 161), 
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at (0, 32).
            anchor: new google.maps.Point(365, 80)
          };        
        // Add Marker
        var marker1 = new google.maps.Marker({
            position: latlng, 
            map: map,       
            icon: image // This path is the custom pin to be shown. Remove this line and the proceeding comma to use default pin
        }); 

        
         
    }
google.maps.event.addDomListener(window, "load", initialize);