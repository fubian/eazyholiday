$(document).ready(function()
{
  //start animation velocity
  $('.small-text-slider').velocity({translateY: -100,opacity: 1, duration: 500});
  $('.big-text-slider').delay(500).velocity({translateY: -100,opacity: 1, duration: 500, sequenceQueue: false});
  $('.button-order-now').delay(800).velocity({translateY: -100,opacity: 1,duration: 500, sequenceQueue: false});
  // main slider plugin
  $('.flexslider').flexslider({
      animation: "fade",
      slideshow: false,
      after: function(){
        $('.small-text-slider').delay(50).velocity({translateY: -100,opacity: 1,  duration: 500, sequenceQueue: false});
        $('.big-text-slider').delay(200).velocity({translateY: -100,opacity: 1, duration: 500, sequenceQueue: false});
        $('.button-order-now').delay(300).velocity({translateY: -100,opacity: 1, duration: 500, sequenceQueue: false});
      },
      before: function(){
        $('.small-text-slider').delay(80).velocity({translateY: 0,opacity: 0,  duration: 500, sequenceQueue: false});
        $('.big-text-slider').delay(100).velocity({translateY: 0,opacity: 0, delay: 250, duration: 500, sequenceQueue: false});
        $('.button-order-now').velocity({translateY: 0,duration: 500, opacity: 0, stagger: 250, sequenceQueue: false});
      },
  });

  //centering modal bootstrap
  var modalVerticalCenterClass = ".modal";
  function centerModals($element) {
    var $modals;
    if ($element.length) {
        $modals = $element;
    } else {
        $modals = $(modalVerticalCenterClass + ':visible');
    }
    $modals.each( function(i) {
        var $clone = $(this).clone().css('display', 'block').appendTo('body');
        var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
        top = top > 0 ? top : 0;
        $clone.remove();
        $(this).find('.modal-content').css("margin-top", top);
    });
  }
  $(modalVerticalCenterClass).on('show.bs.modal', function(e) {
      centerModals($(this));
  });
  $(window).on('resize', centerModals);

  //
  //Date picker
  var nowTemp  = Date.today().addDays(1);
  var now      = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
  var nextTemp = Date.today().next().year().addDays(1);
  var nextYear = new Date(nextTemp.getFullYear(), nextTemp.getMonth(), nextTemp.getDate(), 0, 0, 0, 0);
  var nextDay  = Date.today().addDays(1);
  var ndayDay  = nextDay.getDate();
  var ndayMon  = nextDay.getMonth()+1;
  var ndayYear = nextDay.getFullYear();

  //Date to
  $( '.date-picker-to' ).val( ndayDay + '-' + ndayMon + '-' + ndayYear );
  var dateTo = $( '.date-picker-to' ).datepicker(
  {
    format: 'dd-mm-yyyy',
    beforeShowDay: function(date)
    {
      var dateval = date.valueOf();
      if ( dateval < now.valueOf() || dateval > nextYear.valueOf() ) {
        return 'disabled';
      } else {
        return '';
      }
    }
  })
  .on('changeDate', function(ev)
  {
    dateTo.hide();
  })
  .data('datepicker');

  //Date from
  $( '.date-picker-from' ).val( ndayDay + '-' + ndayMon + '-' + ndayYear );
  var dateFrom = $( '.date-picker-from' ).datepicker(
  {
    format: 'dd-mm-yyyy',
    beforeShowDay: function(date)
    {
      var dateval = date.valueOf();
      if ( dateval < now.valueOf() || dateval > nextYear.valueOf() ) {
        return 'disabled';
      } else {
        return '';
      }
    }
  })
  .on('changeDate', function(ev)
  {
    $('.date-picker-to').datepicker('destroy');
    var stemp = ev.date;
    var sdate = new Date(stemp.getFullYear(), stemp.getMonth(), stemp.getDate(), 0, 0, 0, 0);

    //Change date to
    var toDate = stemp.getDate() + '-' + (stemp.getMonth()+1) + '-' + stemp.getFullYear();
    $( '.date-picker-to' ).val( toDate );
    var dateTo = $( '.date-picker-to' ).datepicker(
    {
      format: 'dd-mm-yyyy',
      beforeShowDay: function(date)
      {
        var dateval = date.valueOf();
        if ( dateval < stemp.valueOf() || dateval > nextYear.valueOf() ) {
          return 'disabled';
        } else {
          return '';
        }
      }
    })
    .on('changeDate', function(ev) {
      dateTo.hide();
    })
    .data('datepicker');

    //Hide datepicker
    dateFrom.hide();
  })
  .data('datepicker');

  //
  //Sekali jalan
  $( '.radio-way-once' ).change( function()
  {
    $( '.wraper-each-input.bottom.date' ).hide();
  });
  $( '.radio-way-twice' ).change( function()
  {
    $( '.wraper-each-input.bottom.date' ).show();
  });

  //
  //Input penumpang
  var maxPassenger = 10;
  $(".passenger-form").keypress(function (e)
  {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      return false;
    }
  });

  //passenger-form focus
  $(".passenger-form").focusin(function (e)
  {
    var input  = $(this);
    var wraper = input.parents( '.wraper-passenger' );
    var total  = _getPassegerTotal();
    var value  = input.val();
    var valInt = new Number( value );

    //Hide all passenger values
    $( '.passenger-values' ).removeClass( 'open' );

    if ( (total - valInt) < maxPassenger )
    {
      var count = maxPassenger - total + valInt;
      var opt   = $( '.passenger-values ul', wraper ).html('');

      for ( var i = 0; i < count; i++ )
      {
        var val = i+1;
        var li  = $('<li data-value="' + val + '">' + val + '</li>');
        li.click( function(e)
        {
          var opt    = $(this).parents( '.passenger-values' );
          var wraper = $(this).parents( '.wraper-passenger' );
          var val    = $(this).data( 'value' );
          //set input
          $( '.passenger-form', wraper ).val( val );
          //Hide opt
          opt.removeClass( 'open' );
          return false;
        });
        opt.append( li );
      }
      $( '.passenger-values', wraper ).addClass( 'open' );
    }
  });
  $(".passenger-form").focusout(function (e)
  {
    var input  = $(this);
    var wraper = input.parents( '.wraper-passenger' );

    //Hide passenger values
    $( '.passenger-values', wraper ).removeClass( 'open' );
  });

  //passenger-form keyup
  $(".passenger-form").keyup(function (e)
  {
    var input  = $(this);
    var wraper = input.parents( '.wraper-passenger' );
    //Clear message
    $( '.passenger-message' ).removeClass( 'right' );
    $( '.passenger-message' ).removeClass( 'center' );
    $( '.passenger-message' ).hide();

    var value = $(this).val();
    if ( value != '' ) {
      $( '.passenger-values', wraper ).removeClass( 'open' );
    } else {
      $( '.passenger-values', wraper ).addClass( 'open' );
    }

    var total = _getPassegerTotal();
    if ( total > maxPassenger )
    {
      input.val('');
      $( '.passenger-message' ).removeClass( 'right' );
      $( '.passenger-message' ).removeClass( 'center' );
      $( '.passenger-message' ).show();

      if ( input.hasClass( 'passenger-child' )) {
        $( '.passenger-message' ).addClass( 'center' );
      }
      else if ( input.hasClass( 'passenger-baby' )) {
        $( '.passenger-message' ).addClass( 'right' );
      }
    }
  });

  //Get total passeger
  function _getPassegerTotal()
  {
    var adult = $('.passenger-adult .passenger-form').val();
    var baby  = $('.passenger-baby .passenger-form').val();
    var child = $('.passenger-child .passenger-form').val();

    var adultInt = babyInt = childInt = 0;
    if ( adult != '0' ) {
      adultInt = new Number( adult );
    }
    if ( baby != '0' ) {
      babyInt = new Number( baby );
    }
    if ( child != '0' ) {
      childInt = new Number( child );
    }
    return adultInt + babyInt + childInt;
  }

  //Input city
  $( '.wraper-input-city' ).each( function()
  {
    var wraper = $(this);

    $( '.input-city-main', wraper ).autocomplete({
      lookup: function (query, done)
      {
        wraper.addClass( 'onload' );

        $.ajax({
          url: 'data/get_cities.php',
          type: "POST",
          dataType: "json",
          data: 'q=' + query,
          success: function (results)
          {
            var data = {
              'suggestions': []
            };
            if ( results )
            {
              for( var i = 0; i < results.length; i++ )
              {
                data.suggestions[i] = {
                  'value': results[i].name,
                  'data': results[i].id
                }
              }
              done( data );
            }
            wraper.removeClass( 'onload' );
          }
        });
      },
      onSelect: function (suggestion) {
        $( '.input-city-value', wraper ).val( suggestion.data );
      }
    });
  });
  
  //
  //Form subscribe
  //

  //name rule
  jQuery.validator.addMethod("validname", function(value, element) {
    return this.optional(element) || /^[a-zA-Z\s]*$/.test(value);
  }, "Nama tidak valid, nama hanya dapat mengandung huruf dan spasi.");

  //Main validate
  $("#form-email-subscribe").validate(
  {
    errorClass: "form-field-error",
    errorElement: "div",
    errorPlacement: function( error, element )
    {
        var field = element.parents( '.form-field' );
        field.addClass( 'form-field-invalid' );
        if ( field.hasClass( 'form-field-inline' )) {
            field.before( error );
        } else {
            element.before( error );
        }
    },
    highlight: function( element, errorClass, validClass )
    {
        $(element)
            .parents( '.form-field' )
            .addClass( 'form-field-invalid' );
    },
    unhighlight: function(element, errorClass, validClass)
    {
      $(element)
          .parents( '.form-field' )
          .removeClass( 'form-field-invalid' );

    },
    onkeyup: false,
    rules: {
      username: {
        required: true,
        validname: true
      },
      email: {
        required: true,
        email: true
      },
    },
    messages: {},
    submitHandler: function(form) {
      form.submit();
    }
 });
//menu mobile navigations
  //function toogle menu when open
    $("#nav-button").click(function(){
        $(this).toggleClass("open");
        $('.main-body').toggleClass('fixed');

    });
  // function on and off sequence when menu clicked
   var buttonOn=false;
   var $push_body = $(".navicon-button"),
       $add_height = $("#mobile-navigation"),
        $push_menu = $(".mobile-list-menu");

    var onSequence = [
        { e: $push_body,  p: {translateX: [100,0]},  o: { duration: 200,  sequenceQueue: false }},
        { e: $add_height,  p: {height: "100%"},  o: { duration: 100,  sequenceQueue: false }},
        { e: $push_menu, p: {translateX: [150,0]}, o: { duration: 350,  delay: 70, sequenceQueue: false }}

    ];

    var offSequence = [

        { e: $push_body, p: {translateX: [0,100]}, o: { duration: 350, sequenceQueue: false }},
        { e: $add_height,  p: {height: "0%"},  o: { duration: 500,  sequenceQueue: false }},
        { e: $push_menu, p: {translateX: [0,150]}, o: { duration: 350, sequenceQueue: false }},


    ];
    // on off button
    $(".navicon-button").click(
      function() {
        buttonOn = !buttonOn;
        if (buttonOn) {
         $.Velocity.RunSequence(onSequence);
        } else {$.Velocity.RunSequence(offSequence);
        }
      });

    $("main-modal-login-click").click(function() {
        $.Velocity.RunSequence(offSequence);
      });
    //smooth scroll
    $(function() {
    $('a[href*="#"]:not([href="#"])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 1000);
          return false;
        }
      }
    });
  });


});
