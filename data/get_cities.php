<?php
$cities = array(
  array(
    "id"   => 1,
    "name" => "Aceh",
  ),
  array(
    "id"   => 2,
    "name" => "Sumatra Utara",
  ),
  array(
    "id"   => 3,
    "name" => "Sumatra Barat",
  ),
  array(
    "id"   => 4,
    "name" => "Sumatra Selatan",
  ),
  array(
    "id"   => 5,
    "name" => "Bengkulu",
  ),
  array(
    "id"   => 6,
    "name" => "DKI Jakarta",
  ),
  array(
    "id"   => 7,
    "name" => "Jawa Barat",
  ),
  array(
    "id"   => 8,
    "name" => "Jawa Tengah",
  ),
  array(
    "id"   => 9,
    "name" => "DIY Yogyakarta",
  ),
  array(
    "id"   => 10,
    "name" => "Jawa Timur",
  ),
  array(
    "id"   => 11,
    "name" => "Bali",
  ),
  array(
    "id"   => 12,
    "name" => "Kalimantan Barat",
  ),
  array(
    "id"   => 13,
    "name" => "Kalimantan Tengah",
  ),
  array(
    "id"   => 14,
    "name" => "Kalimantan Timur",
  ),
  array(
    "id"   => 15,
    "name" => "Sulawesi Barat",
  )
);

echo json_encode( $cities );
?>
