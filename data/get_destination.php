<?php
$cities = array(
  array(
    "id"   => 1,
    "name" => "Aceh",
    "type" => "City"
  ),
  array(
    "id"   => 2,
    "name" => "Sumatra Utara",
    "type" => "City"
  ),
  array(
    "id"   => 3,
    "name" => "Jambu Luwuk",
    "type" => "Hotel"
  ),
  array(
    "id"   => 4,
    "name" => "Sumatra Selatan",
    "type" => "City"
  ),
  array(
    "id"   => 5,
    "name" => "Pop Hotel",
    "type" => "Hotel"
  ),
  array(
    "id"   => 6,
    "name" => "Parangtritis",
    "type" => "Place"
  ),
  array(
    "id"   => 7,
    "name" => "Jawa Barat",
    "type" => "City"
  ),
  array(
    "id"   => 8,
    "name" => "Crystal Palace",
    "type" => "Hotel"
  ),
  array(
    "id"   => 9,
    "name" => "Ibis Hotel",
    "type" => "Hotel"
  ),
  array(
    "id"   => 10,
    "name" => "Malioboro",
    "type" => "Place"
  ),
  array(
    "id"   => 11,
    "name" => "Pantai Wediombo",
    "type" => "Place"
  ),
  array(
    "id"   => 12,
    "name" => "Kalimantan Barat",
    "type" => "City"
  ),
  array(
    "id"   => 13,
    "name" => "Kalimantan Tengah",
    "type" => "City"
  ),
  array(
    "id"   => 14,
    "name" => "Sahid",
    "type" => "Hotel"
  ),
  array(
    "id"   => 15,
    "name" => "Ambarukmo",
    "type" => "Hotel"
  )
);

echo json_encode( $cities );
?>
